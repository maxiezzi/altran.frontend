import React, { Component, useState, useEffect } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import { Switch, Route } from 'react-router-dom';
import { Container, Spinner } from 'reactstrap';
import CensusGenomeCardList from './components/CensusGenomeCardList/CensusGenomeCardList';
import Navbar from './components/NavBar';
import GenomeDetails from './components/GnomeDetails/GnomeDetails';
import Axios from 'axios';
import { censusGnomeLoad, filterProfessionLoad } from './redux/Actions/CensusGenomeCardListAction';
import { connect, useDispatch } from "react-redux";

const AppRender = () => {
  return (
    <Container fluid>
      <Navbar />
      <Switch>
        <Route exact path="/" component={(props) => <CensusGenomeCardList {...props} />} />
        <Route exact path="/gnomeDetails/:id" component={(props) => <GenomeDetails {...props} />} />
        <Route exact path="/Pages" component={() => "Pages"} />
        <Route exact path="/faq" component={() => "FAQ"} />
        <Route exact path="/contact" component={() => "Contact"} />
        <Route exact path="/Page-1" component={(props) => <CensusGenomeCardList {...props} />} />
        <Route exact path="/Page-2" component={(props) => <GenomeDetails {...props} />} />
        <Route exact path="/Home-3" component={() => "Home-3"} />
        <Route exact path="/Page-1" component={() => "Page-1"} />
        <Route exact path="/Page-2" component={() => "Page-2"} />
        <Route exact path="/page-1" component={() => "page-1"} />
        <Route exact path="/page-2" component={() => "page-2"} />
        <Route exact path="/page-3" component={() => "page-3"} />
        <Route exact path="/page-4" component={() => "page-4"} />
      </Switch>
    </Container>
  );
}

const App = () => {
  const dispatch = useDispatch();

  const [isLoaded, setIsLoaded] = useState(false);

  useEffect(() => {
    Axios.get(`${process.env.PUBLIC_URL}/Census.json`)
      .then(response => {
        let isGenderMale = true;
        response.data.Brastlewark.forEach(gnome => {
          gnome.gender = isGenderMale ? 'male' : 'female'
          isGenderMale = !isGenderMale
        })

        dispatch(censusGnomeLoad(response.data.Brastlewark));

        let mySet = new Set();
        response.data.Brastlewark.forEach(gnome =>
          gnome.professions.forEach(profession =>
            mySet.add(profession)));

        dispatch(filterProfessionLoad(Array.from(mySet)));

        setIsLoaded(true);
      });
  }, [isLoaded]);


  if (!isLoaded) {
    return (
      <div className="d-flex justify-content-center text-center align-middle">
        <Spinner color="primary" className="text-center" style={{ width: '18rem', height: '18rem' }} value="Loading">

        </Spinner>
        Loading
        </div>
    );
  } else {
    return (
      <AppRender />
    )
  }
}


export default App;