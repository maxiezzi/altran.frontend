import React from 'react';
import {
    Card, CardImg, CardText, CardBody,
    CardTitle, CardSubtitle, Row, Col
} from 'reactstrap';
import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPlus } from '@fortawesome/free-solid-svg-icons'
import { Link } from 'react-router-dom';
import './GnomeCard.css'

const GnomeCardRender = (props) => {
    return (
        <Card className="GnomeCard">
            <div className="row no-gutters">
                <div className="col-auto imgContent">
                    <CardImg id="thumbnail" className="cardImg" src={props.thumbnail} alt="Card image cap" />
                </div>
                <div className="col">
                    <CardBody>
                        <CardTitle>
                            <Row>
                                <Col md={10}>
                                    <strong id="name">
                                        {props.name}
                                    </strong>
                                </Col>
                                <Col md={2}>
                                    <Link style={{ float: 'right' }}
                                        id={'gnomeCard_' + props.id}
                                        onClick={() => props.onClick(props.id)}
                                        to={{ pathname: '/gnomeDetails/' + props.id }}
                                        className="btn btn-outline-info btn-sm"
                                        title="More Info" >
                                        <FontAwesomeIcon icon={faPlus} />
                                    </Link>
                                </Col>
                            </Row>
                        </CardTitle>
                        <CardSubtitle id="age"><strong>Age</strong>: {props.age} years</CardSubtitle>
                        <CardText id="professions"><strong>Professions</strong>: {props.professions.join(', ')}</CardText>
                    </CardBody>
                </div>
            </div>
        </Card>
    )
};

GnomeCardRender.propTypes = {
    name: PropTypes.string,
    age: PropTypes.number,
    professions: PropTypes.array,
    thumbnail: PropTypes.string,
    id: PropTypes.number,
    onClick: PropTypes.func
};

export default GnomeCardRender;