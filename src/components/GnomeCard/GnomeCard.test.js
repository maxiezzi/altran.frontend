import React from 'react';
import GnomeCardRender from './GnomeCard';
import { censusGnome } from '../../__test__/const';
import { shallow } from 'enzyme';
import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure({ adapter: new Adapter() });

describe('GnomeCardRender', () => {
    let gnome = censusGnome[0];
    let component = shallow(
        <GnomeCardRender
            key={gnome.id}
            name={gnome.name}
            age={gnome.age}
            professions={gnome.professions}
            thumbnail={gnome.thumbnail}
            id={gnome.id}
        />);
    test('Validate Age', () => {
        var value = component.find('#age').props().children[2];
        expect(gnome.age).toBe(value);
    });

    test('Validate name', () => {
        var value = component.find('#name').props().children;
        expect(gnome.name).toBe(value);
    });

    test('Validate professions', () => {
        var value = component.find('#professions').props().children[2];
        expect(gnome.professions.join(', ')).toBe(value);
    });
    
})
