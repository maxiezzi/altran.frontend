import React from 'react';
import Select from 'react-select';
import makeAnimated from 'react-select/animated';
import PropTypes from 'prop-types';
import { FormGroup, Label } from 'reactstrap';
import { filterGender } from '../../redux/Actions/FilterSeccionAction';
import { useDispatch } from "react-redux";

const animatedComponents = makeAnimated();

const options = [
    {
        value: '',
        label: 'All'
    },
    {
        value: 'Male',
        label: 'Male'
    },
    {
        value: 'Female',
        label: 'Female'
    }
]

const GenderFilter = (props) => {
    const dispatch = useDispatch();    

    let item = {
        value: props.defaultValue === '' ? '' : props.defaultValue,
        label: props.defaultValue === '' ? '' : props.defaultValue,
    }
    return (
        <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
            <Label for="genderFilter" className="mr-sm-2">Gender</Label>
            <Select
                id="genderFilter"
                closeMenuOnSelect={false}
                defaultValue={item}
                components={animatedComponents}
                options={options}
                onChange={(object) => dispatch(filterGender(object === null ? '' : object.value))}
            />
        </FormGroup>
    )
}

GenderFilter.propTypes = {
    defaultValue: PropTypes.string,
    onChange: PropTypes.func
}

export default GenderFilter;