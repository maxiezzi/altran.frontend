import React from "react";
import GenderFilter from "./GenderFilter";
import { mount, shallow } from 'enzyme';
import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { Provider } from 'react-redux';

import FilterReducer from '../../redux/Reducers/FilterReducer';
import { filterGender } from '../../redux/Actions/FilterSeccionAction';
import { createStore } from 'redux';

configure({ adapter: new Adapter() });

describe('GenderFilter', () => {
    const initialState = {
        professions: null,
        filtersSelected: [],
        name: '',
        orderBy: '',
        gender: ''
    };
    const mockStore = createStore(FilterReducer, initialState);

    it("onChange Event - Male", () => {
        const event = {
            value: 'Male'
        };

        mockStore.dispatch = jest.fn();
        const component = mount(
            <Provider store={mockStore} >
                <GenderFilter />
            </Provider>);

        component.find('#genderFilter').at(0).props().onChange(event);
        expect(mockStore.dispatch).toBeCalledWith(filterGender('Male'));
    });

    it("onChange Event - Female", () => {
        const event = {
            value: 'Female'
        };

        mockStore.dispatch = jest.fn();
        const component = mount(
            <Provider store={mockStore} >
                <GenderFilter />
            </Provider>);

        component.find('#genderFilter').at(0).props().onChange(event);
        expect(mockStore.dispatch).toBeCalledWith(filterGender('Female'));
    });

    it("Default Value - Male", () => {
        const value = {
            value: 'Male',
            label: 'Male',
        }
        const component = mount(
            <Provider store={mockStore} >
                <GenderFilter defaultValue={value.value} />
            </Provider>);

        var defaultValue = component.find('#genderFilter').at(0).props().defaultValue;
        expect(defaultValue).toMatchObject(value);
    });

    it("Default Value - Female", () => {
        const value = {
            value: 'Female',
            label: 'Female',
        }
        const component = mount(
            <Provider store={mockStore} >
                <GenderFilter defaultValue={value.value} />
            </Provider>);

        var defaultValue = component.find('#genderFilter').at(0).props().defaultValue;
        expect(defaultValue).toMatchObject(value);
    });
});