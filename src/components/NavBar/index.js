import React, { useState } from 'react';
import { Navbar, NavbarToggler, Nav, NavItem, NavLink } from 'reactstrap';
import { Link } from 'react-router-dom';

export default props => {

  const [isOpen, setOpen] = useState(true)
  const toggle = () => setOpen(!isOpen)

  return (
    <Navbar color="light" light className="navbar shadow-sm p-3 mb-5 rounded" expand="xs">
      <NavbarToggler onClick={toggle} />
      
        <Nav className="ml-auto" navbar>
          <NavItem>
            <NavLink tag={Link} to={'/'}>Home</NavLink>
          </NavItem>
        </Nav>
      
    </Navbar>
  );
}
