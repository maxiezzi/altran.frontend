import React, { useCallback } from 'react';
import PropTypes from 'prop-types';
import { Input, FormGroup, Label } from 'reactstrap';
import { filterName } from '../../redux/Actions/FilterSeccionAction';
import { useDispatch } from "react-redux";


const NameFilter = (props) => {
    const dispatch = useDispatch();

    return (
        <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
            <Label for="nameFilter" className="mr-sm-2">Name</Label>
            <Input type="text" name="nameFilter" id="nameFilter"
                onChange={(event) => dispatch(filterName(event.target.value))}
                value={props.defaultValue}
            />
        </FormGroup>
    )
}

NameFilter.propTypes = {
    onChange: PropTypes.func,
    defaultValue: PropTypes.string
}

export default NameFilter;