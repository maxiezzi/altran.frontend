import React from "react";
import NameFilter from "./NameFilter";
import { mount } from 'enzyme';
import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { Provider } from 'react-redux'

import FilterReducer from '../../redux/Reducers/FilterReducer'
import { filterName } from '../../redux/Actions/FilterSeccionAction'
import { createStore } from 'redux';

configure({ adapter: new Adapter() });

describe('NameFilter', () => {

    const initialState = {
        professions: null,
        filtersSelected: [],
        name: '',
        orderBy: '',
        gender: ''
    };
    const mockStore = createStore(FilterReducer, initialState);

    it("onChange Event", () => {
        const event = {
            target: { value: 'the-value' }
        };
        mockStore.dispatch = jest.fn();


        const component = mount(
            <Provider store={mockStore} >
                <NameFilter defaultValue={"default"} />
            </Provider>);

        component.find('#nameFilter').at(0).simulate('change', event);
        expect(mockStore.dispatch).toBeCalledWith(filterName('the-value'));
    });

    it("Default Value", () => {
        const component = mount(
            <Provider store={mockStore} >
                <NameFilter defaultValue={"default"} />
            </Provider>);

        var defaultValue = component.find('#nameFilter').at(0).props().value;
        expect(defaultValue).toBe("default");
    });
});