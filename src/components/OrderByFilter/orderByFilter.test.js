import React from "react";
import OrderByFilter from "./orderByFilter";
import { mount } from 'enzyme';
import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { Provider } from 'react-redux';

import FilterReducer from '../../redux/Reducers/FilterReducer';
import { filterOrderBy } from '../../redux/Actions/FilterSeccionAction';
import { createStore } from 'redux';

configure({ adapter: new Adapter() });
const options = [
    {
        value: 'None',
        label: 'None'
    },
    {
        value: 'Age Desc',
        label: 'Age Desc'
    },
    {
        value: 'Age Asc',
        label: 'Age Asc'
    },
    {
        value: 'Height Desc',
        label: 'Height Desc'
    },
    {
        value: 'Height Asc',
        label: 'Height Asc'
    },
    {
        value: 'Weight Desc',
        label: 'Weight Desc'
    },
    {
        value: 'Weight Asc',
        label: 'Weight Asc'
    },
    {
        value: 'Friend Desc',
        label: 'Friend Desc'
    },
    {
        value: 'Friend Asc',
        label: 'Friend Asc'
    },
    {
        value: 'Professions Desc',
        label: 'Professions Desc'
    },
    {
        value: 'Professions Asc',
        label: 'Professions Asc'
    }

]
describe('OrderByFilter', () => {
    const initialState = {
        professions: null,
        filtersSelected: [],
        name: '',
        orderBy: '',
        gender: ''
    };
    const mockStore = createStore(FilterReducer, initialState);

    it("onChange Event - All", () => {
        const event = {
            value: options[0].value
        };
        mockStore.dispatch = jest.fn();
        const component = mount(
            <Provider store={mockStore} >
                <OrderByFilter />
            </Provider>);

        component.find('#orderByFilter').at(0).props().onChange(event);
        expect(mockStore.dispatch).toBeCalledWith(filterOrderBy(options[0].value));
    });mockStore.dispatch

    it("Default Value - All", () => {
        const value = options[0];

        const component = mount(
            <Provider store={mockStore} >
                <OrderByFilter defaultValue={value.value} />
            </Provider>);
        
        var defaultValue = component.find('#orderByFilter').at(0).props().defaultValue;
        expect(defaultValue).toMatchObject(value);

    });
});