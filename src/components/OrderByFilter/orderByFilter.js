import React from 'react';

import Select from 'react-select';
import makeAnimated from 'react-select/animated';
import PropTypes from 'prop-types';
import { FormGroup, Label } from 'reactstrap';
import { filterOrderBy } from '../../redux/Actions/FilterSeccionAction';
import { useDispatch } from "react-redux";

const animatedComponents = makeAnimated();

const options = [
    {
        value: 'None',
        label: 'None'
    },
    {
        value: 'Age Desc',
        label: 'Age Desc'
    },
    {
        value: 'Age Asc',
        label: 'Age Asc'
    },
    {
        value: 'Height Desc',
        label: 'Height Desc'
    },
    {
        value: 'Height Asc',
        label: 'Height Asc'
    },
    {
        value: 'Weight Desc',
        label: 'Weight Desc'
    },
    {
        value: 'Weight Asc',
        label: 'Weight Asc'
    },
    {
        value: 'Friend Desc',
        label: 'Friend Desc'
    },
    {
        value: 'Friend Asc',
        label: 'Friend Asc'
    },
    {
        value: 'Professions Desc',
        label: 'Professions Desc'
    },
    {
        value: 'Professions Asc',
        label: 'Professions Asc'
    }

]

const OrderByFilter = (props) => {
    const dispatch = useDispatch();

    let item = {
        value: props.defaultValue === '' ? '' : props.defaultValue,
        label: props.defaultValue === '' ? '' : props.defaultValue,
    }

    return (
        <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
            <Label for="orderByFilter" className="mr-sm-2">Order By</Label>
            <Select
                id="orderByFilter"
                name="orderByFilter"
                closeMenuOnSelect={false}
                defaultValue={item}
                components={animatedComponents}
                options={options}
                onChange={(object) => dispatch(filterOrderBy(object === null ? '' : object.value))}
            />
        </FormGroup>
    )
}

OrderByFilter.propTypes = {
    defaultValue: PropTypes.string,
    onChange: PropTypes.func
}

export default OrderByFilter;