import React from 'react';
import GnomeDetailsRender from './GnomeDetailsRender';
import { censusGnome } from '../../__test__/const';
import { shallow } from 'enzyme';
import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure({ adapter: new Adapter() });

describe('GenomeDetailsRender', () => {
    let gnome = censusGnome[0];
    let component = shallow(<GnomeDetailsRender
        age={gnome.age}
        friends={gnome.friends}
        hairColor={gnome.hair_color}
        height={gnome.height.toString()}
        name={gnome.name}
        professions={gnome.professions}
        thumbnail={gnome.thumbnail}
        weight={gnome.weight.toString()}
        gender={gnome.gender}
    />);
    test('Validate Age', () => {
        var value = component.find('#age').props().defaultValue;
        expect(`${gnome.age} years.`).toBe(value);
    });

    test('Validate weight', () => {
        var value = component.find('#weight').props().defaultValue;
        expect(`${gnome.weight} kg.`).toBe(value);
    });

    test('Validate height', () => {
        var value = component.find('#height').props().defaultValue;
        expect(`${gnome.height} cm.`).toBe(value);
    });

    test('Validate hairColor', () => {
        var value = component.find('#hairColor').props().defaultValue;
        expect(gnome.hair_color).toBe(value);
    });
    
    test('Validate professions', () => {
        var value = component.find('#professions').props().children.length;
        expect(gnome.professions.length).toBe(value);
    });
    
    test('Validate without friends', () => {
        let gnomeWithoutFriends = censusGnome[1];
        let componentWithouFriends = shallow(<GnomeDetailsRender
            age={gnomeWithoutFriends.age}
            friends={gnomeWithoutFriends.friends}
            hairColor={gnomeWithoutFriends.hair_color}
            height={gnomeWithoutFriends.height.toString()}
            name={gnomeWithoutFriends.name}
            professions={gnomeWithoutFriends.professions}
            thumbnail={gnomeWithoutFriends.thumbnail}
            weight={gnomeWithoutFriends.weight.toString()}
            gender={gnomeWithoutFriends.gender}
        />);
        var value = componentWithouFriends.find('#friends').props().children.length;
        expect(gnomeWithoutFriends.friends.length).toBe(value);
    });
})
