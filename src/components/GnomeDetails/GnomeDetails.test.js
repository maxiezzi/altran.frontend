import React from 'react';
import {Provider} from 'react-redux'
import renderer from 'react-test-renderer';
import configureStore from 'redux-mock-store'
import GenomeDetails, { ConnectedGenomeDetails } from './GnomeDetails';
import { censusGnome } from '../../__test__/const';

describe('GenomeDetails', () => {
    test('with gnomeSelected', () => {
        const initialState = {
            censusGenome: {
                gnomeSelected: censusGnome[0]
            }
        }
        const mockStore = configureStore()
        let store = mockStore(initialState);

        const component = renderer.create(
            <Provider store={store}>
                <GenomeDetails/>,
            </Provider>
        );
        let tree = component.toJSON();
        expect(tree).toMatchSnapshot();
    });

    test('without gnomeSelected', () => {
        const initialState = {
            censusGenome: {
                all: censusGnome,
                gnomeSelected: null
            }
        }
        const match = {
            params: {
                id: 0
            }
        }


        const mockStore = configureStore();
        let store = mockStore(initialState);

        const component = renderer.create(
            <Provider store={store}>
                <GenomeDetails match={match} />,
            </Provider>
        );
        let tree = component.toJSON();
        expect(tree).toMatchSnapshot();
    });
})