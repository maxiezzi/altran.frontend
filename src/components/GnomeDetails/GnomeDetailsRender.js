import React from 'react';
import PropTypes from 'prop-types';
import { Col, Input, FormGroup, Label, Row, CardImg, CardTitle, ListGroup, CardBody, ListGroupItem } from 'reactstrap';
import './GnomeDetails.css'

const GnomeDetailsRender = (props) => (
    <div className="mb-3" style={{ maxHeight: "540px" }}>
        <Row noGutters>
            <Col md={5}>
                <CardImg 
                    className="cardImagen"
                    top 
                    src={props.thumbnail} 
                    alt="Card image cap" />
            </Col>
            <Col md={7}>
                <CardBody>
                    <CardTitle>
                        <strong>{props.name}</strong> ({props.gender})
                    </CardTitle>
                    <Row form>
                        <Col md={6}>
                            <FormGroup row>
                                <Label for="weight" sm={2}><strong>weight: </strong></Label>
                                <Col sm={10}>
                                    <Input id="weight" plaintext defaultValue={props.weight + ' kg.'} />
                                </Col>
                            </FormGroup>
                        </Col>
                        <Col md={6}>
                            <FormGroup row>
                                <Label for="height" sm={2}><strong>Height:</strong> </Label>
                                <Col sm={10}>
                                    <Input id="height" plaintext defaultValue={props.height + ' cm.'} />
                                </Col>
                            </FormGroup>
                        </Col>
                    </Row>
                    <Row form>
                        <Col md={6}>
                            <FormGroup row>
                                <Label for="age" sm={2}><strong>Age: </strong></Label>
                                <Col sm={10}>
                                    <Input id="age" plaintext defaultValue={props.age + ' years.'} />
                                </Col>
                            </FormGroup>
                        </Col>
                        <Col md={6}>
                            <FormGroup row>
                                <Label for="hairColor" sm={2}><strong>Hair Color:</strong> </Label>
                                <Col sm={10}>
                                    <Input id="hairColor" plaintext defaultValue={props.hairColor} />
                                </Col>
                            </FormGroup>
                        </Col>
                    </Row>
                    <Row>
                        <Col md={6}>
                            <p><strong>Professions</strong></p>
                            <ListGroup id="professions">
                                {
                                    props.professions === undefined ? <div/> :
                                    props.professions.map((profession, index) =>
                                        <ListGroupItem key={index}>{profession}</ListGroupItem>
                                    )
                                }
                            </ListGroup>
                        </Col>
                        <Col md={6}>
                            <p><strong>Friends</strong></p>
                            <ListGroup id="friends">
                                {
                                    props.friends === undefined ? <div/> :
                                    props.friends.map((friend, index) =>
                                        <ListGroupItem key={index}>{friend}</ListGroupItem>
                                    )
                                }
                            </ListGroup>
                        </Col>
                    </Row>
                </CardBody>
            </Col>
        </Row>
    </div >
);

GnomeDetailsRender.propTypes = {
    name: PropTypes.string,
    age: PropTypes.number,
    professions: PropTypes.array,
    thumbnail: PropTypes.string,
    weight: PropTypes.string,
    height: PropTypes.string,
    hairColor: PropTypes.string,
    friends: PropTypes.array,
    gender: PropTypes.string
};

export default GnomeDetailsRender;