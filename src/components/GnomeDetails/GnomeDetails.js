import React, { Component } from 'react';
import { connect } from "react-redux";
import GnomeDetailsRender from './GnomeDetailsRender';
import { useSelector } from 'react-redux';

const GenomeDetails = (props) => {
    var gnomeSelected = useSelector(state => state.censusGenome.gnomeSelected);
    const censusGnome = useSelector(state => state.censusGenome.all);

    if(gnomeSelected === null){
        gnomeSelected = censusGnome.find(gnome => gnome.id === Number(props.match.params.id));
    }

    return (
        <GnomeDetailsRender
            age={gnomeSelected.age}
            friends={gnomeSelected.friends}
            hairColor={gnomeSelected.hair_color}
            height={gnomeSelected.height.toString()}
            name={gnomeSelected.name}
            professions={gnomeSelected.professions}
            thumbnail={gnomeSelected.thumbnail}
            weight={gnomeSelected.weight.toString()}
            gender={gnomeSelected.gender}
        />
    );
}

export default GenomeDetails;