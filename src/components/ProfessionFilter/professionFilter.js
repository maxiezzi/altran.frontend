import React from 'react';

import Select from 'react-select';
import makeAnimated from 'react-select/animated';
import PropTypes from 'prop-types';
import { FormGroup, Label } from 'reactstrap';
import { filterSelected } from '../../redux/Actions/FilterSeccionAction';
import { useDispatch } from "react-redux";

const animatedComponents = makeAnimated();

const ProfessionsFilter = (props) => {
    const dispatch = useDispatch();

    var item = null;
    const options = props.professions.map(professions =>
        item = {
            value: professions,
            label: professions
        }
    );
    const defaultSelect = props.defaultValue.length === 0 ? [] : props.defaultValue.map(professions =>
        item = {
            value: professions,
            label: professions
        }
    );
    return (
        <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
            <Label for="professionFilter" className="mr-sm-2">Professions</Label>
            <Select
                id="professionFilter"
                closeMenuOnSelect={false}
                defaultValue={defaultSelect}
                components={animatedComponents}
                isMulti
                options={options}
                onChange={(object) => dispatch(filterSelected(object === null ? [] : object.map(item => item.value)))}
            />
        </FormGroup>
    )
}

ProfessionsFilter.propTypes = {
    professions: PropTypes.array,
    defaultValue: PropTypes.array,
    onChange: PropTypes.func
}

export default ProfessionsFilter;