import React from "react";
import ProfessionFilter from "./professionFilter";
import { shallow } from 'enzyme';
import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

const useDispatchMock = jest.fn();
jest.mock('react-redux', () => ({
    useDispatch: ()=> useDispatchMock,
}));

configure({ adapter: new Adapter() });
const profession = [
    "Metalworker",
    "Woodcarver",
    "Potter"
]
describe('ProfessionFilter', () => {
    it("onChange Event - All", () => {
        profession.forEach((e) => {
            const event = {
                target: { 
                    object: e
                }
            };
            const component = shallow(<ProfessionFilter professions={profession} defaultValue={[]}/>);
            component.find('#professionFilter').simulate('change', event);
            expect(useDispatchMock).toBeCalledWith(event);
        })
    });

    it("Default Value - MetalWorker", () => {
        const Object = [
            {
                label: "MetalWorker",
                value: "MetalWorker"
            }
        ];
        const value = [
            "MetalWorker"
        ];
        const component = shallow(<ProfessionFilter professions={profession} defaultValue={value} />);
        var defaultValue = component.find('#professionFilter').props().defaultValue;
        expect(defaultValue).toMatchObject(Object);
    });

    it("Default Value - MetalWorker and Potter", () => {
        const Object = [
            {
                label: "MetalWorker",
                value: "MetalWorker"
            },
            {
                label: "Potter",
                value: "Potter"
            }
        ];
        const value = [
            "MetalWorker",
            "Potter"
        ];
        const component = shallow(<ProfessionFilter professions={profession} defaultValue={value} />);
        var defaultValue = component.find('#professionFilter').props().defaultValue;
        expect(defaultValue).toMatchObject(Object);
    });
});