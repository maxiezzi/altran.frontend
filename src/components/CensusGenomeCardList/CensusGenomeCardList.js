import React, { Component, useState, useEffect } from 'react';
import { Row, Col, Spinner } from 'reactstrap'
import GnomeCardRender from '../GnomeCard/GnomeCard';
import { censusGnomeLoad, gnomeSelected, filterProfessionLoad } from '../../redux/Actions/CensusGenomeCardListAction';
import { connect, useSelector, useDispatch } from "react-redux";
import FilterSeccion from '../FilterSeccion/FilterSeccion';
import InfiniteScroll from "react-infinite-scroll-component";
import './CensusGenomeCardList.css'

const CensusGenomeCardList = (props) => {
    const dispatch = useDispatch();

    //selector
    const censusGenome = useSelector(state => state.censusGenome.all);
    const professionFilter = useSelector(state => state.filters.filtersSelected);
    const nameFilter = useSelector(state => state.filters.name);
    const orderByFilter = useSelector(state => state.filters.orderBy);
    const genderFilter = useSelector(state => state.filters.gender);

    //states
    const [isLoaded, setIsLoaded] = useState(false);
    const [population, setPopulation] = useState([]);
    const [populationInScroll, setPopulationInScroll] = useState([]);
    const [hasMore, setHasMore] = useState(true);
    const [index, setIndex] = useState(1);

    const fetchMoreData = () => {
        if (populationInScroll.length === population.length) {
            setHasMore(false);
            return;
        }
        // a fake async api call like which sends
        // 20 more records in .5 secs
        setTimeout(() => {
            setPopulationInScroll(population.slice(0, 50 * index));
            setIndex(index + 1);
        }, 500);
    };

    const onClick = (id) => {
        dispatch(gnomeSelected(censusGenome.find(gnome => gnome.id === id)));
    }

    const applyFilters = () => {
        let populationFiltered = censusGenome

        if (professionFilter !== undefined && professionFilter.length !== 0) {
            populationFiltered = populationFiltered.filter(gnome =>
                professionFilter.every(profession =>
                    gnome.professions.includes(profession)
                )
            )
        }

        if (nameFilter !== '') {
            populationFiltered = populationFiltered.filter(gnome =>
                gnome.name.toLowerCase().includes(nameFilter.toLowerCase())
            )
        }

        if (genderFilter !== '') {
            populationFiltered = populationFiltered.filter(gnome =>
                gnome.gender.toLowerCase() === genderFilter.toLowerCase()
            )
        }

        if (orderByFilter !== '') {
            switch (orderByFilter) {
                case 'Age Desc':
                    populationFiltered = populationFiltered.sort((a, b) => b.age - a.age);
                    break;
                case 'Age Asc':
                    populationFiltered = populationFiltered.sort((a, b) => a.age - b.age);
                    break;
                case 'Weight Desc':
                    populationFiltered = populationFiltered.sort((a, b) => b.weight - a.weight);
                    break;
                case 'Weight Asc':
                    populationFiltered = populationFiltered.sort((a, b) => a.weight - b.weight);
                    break;
                case 'Height Desc':
                    populationFiltered = populationFiltered.sort((a, b) => b.height - a.height);
                    break;
                case 'Height Asc':
                    populationFiltered = populationFiltered.sort((a, b) => a.height - b.height);
                    break;
                case 'Friend Desc':
                    populationFiltered = populationFiltered.sort((a, b) => b.friends.length - a.friends.length);
                    break;
                case 'Friend Asc':
                    populationFiltered = populationFiltered.sort((a, b) => a.friends.length - b.friends.length);
                    break;
                case 'Professions Desc':
                    populationFiltered = populationFiltered.sort((a, b) => b.professions.length - a.professions.length);
                    break;
                case 'Professions Asc':
                    populationFiltered = populationFiltered.sort((a, b) => a.professions.length - b.professions.length);
                    break;
                default:

            }
        }

        return populationFiltered;
    }

    useEffect(() => {
        if (censusGenome != null) {
            const populationFiltered = applyFilters();

            setIsLoaded(true);
            setPopulation(populationFiltered);
            setPopulationInScroll(populationFiltered.slice(0, 25));
        }
    }, [isLoaded]);

    useEffect(() => {

        const populationFiltered = applyFilters()

        setHasMore(populationFiltered.length > 25);
        setIndex(1);
        setPopulation(populationFiltered);
        setPopulationInScroll(populationFiltered.slice(0, 25));
    }, [genderFilter, orderByFilter, nameFilter, professionFilter]);

    if (!isLoaded) {
        return (
            <div className="d-flex justify-content-center text-center align-middle">
                <Spinner color="primary" className="text-center" style={{ width: '18rem', height: '18rem' }} value="Loading">

                </Spinner>
        </div>
        )
    } else {
        const CartList = populationInScroll.map(person => (
            <Col key={person.id} md={4} style={{ marginBottom: '10px' }}>
                <GnomeCardRender
                    key={person.id}
                    name={person.name}
                    age={person.age}
                    professions={person.professions}
                    thumbnail={person.thumbnail}
                    id={person.id}
                    onClick={onClick}
                />
            </Col>
        ));
        return (
            <div>
                <FilterSeccion />
                <br />

                <InfiniteScroll
                    dataLength={populationInScroll.length}
                    next={fetchMoreData}
                    hasMore={hasMore && population.length > 25}
                    loader={<Spinner color="primary" className="text-center" />}
                >
                    <Row>
                        {CartList}
                    </Row>
                </InfiniteScroll>
            </div>
        );
    }
}


/*
export class ConnectedCensusGenomeCardList extends Component {

    

    

    componentWillReceiveProps(props) {
        const populationFiltered = this.applyFilters(props)
        this.setState({
            reload: false,
            population: populationFiltered,
            populationInScroll: populationFiltered.slice(0, 25),
            index: 1,
            hasMore: true
        });
    }
*/
export default CensusGenomeCardList;