import React, { useCallback, Component } from 'react';
import { filterSelected, filterName, filterOrderBy, filterGender } from '../../redux/Actions/FilterSeccionAction';
import { connect, useDispatch, useSelector } from "react-redux";
import ProfessionsFilter from './../ProfessionFilter/professionFilter';
import NameFilter from '../NameFilter/NameFilter';
import OrderByFilter from '../OrderByFilter/orderByFilter';
import { Form, Row, Col } from 'reactstrap';
import GenderFilter from '../GenderFilter/GenderFilter';

const FilterSeccion = (props) => {
    const professions = useSelector(state => state.filters.professions);
    const nameFilter = useSelector(state => state.filters.name);
    const professionFilter = useSelector(state => state.filters.filtersSelected);
    const orderByFilter = useSelector(state => state.filters.orderBy);
    const genderFilter = useSelector(state => state.filters.gender);

    return (
        <Form>
            <Row form>
                <Col md={6}>
                    <ProfessionsFilter
                        professions={professions}
                        defaultValue={professionFilter}
                    />
                </Col>
                <Col md={2}>
                    <NameFilter
                        defaultValue={nameFilter}
                    />
                </Col>
                <Col md={2}>
                    <GenderFilter
                        defaultValue={genderFilter}
                    />
                </Col>
                <Col md={2}>
                    <OrderByFilter
                        defaultValue={orderByFilter}
                    />
                </Col>
            </Row>
        </Form>
    );
}

export default FilterSeccion;
