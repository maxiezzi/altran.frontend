import { censusGnome, } from '../../__test__/const';
import CensusGnomeReducer from './CensusGnomeReducer'
import { CENSUS_GNOME_LOAD, GNOME_SELECTED } from '../Utils/Constant';

describe('CensusGnome reducer', () => {
    it('should return the initial state', () => {
        expect(CensusGnomeReducer(undefined, {})).toEqual(
            {
                all: null,
                gnomeSelected: null
            }
        )
    })

    it('should handle CENSUS_GNOME_LOAD', () => {
        expect(
            CensusGnomeReducer([], {
                type: CENSUS_GNOME_LOAD,
                censusGnome: censusGnome
            })
        ).toEqual(
            {
                all: censusGnome,
                gnomeSelected: undefined
            }
        )
    })

    it('should handle GNOME_SELECTED', () => {
        expect(
            CensusGnomeReducer([], {
                type: GNOME_SELECTED,
                gnome: censusGnome[0]
            })
        ).toEqual(
            {
                all: undefined,
                gnomeSelected: censusGnome[0]
            }
        )
    })
})