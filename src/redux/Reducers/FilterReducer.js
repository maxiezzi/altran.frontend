import { FILTER_PROFESSION_LOAD, FILTER_SELECTED, FILTER_NAME, FILTER_ORDER_BY } from "../Utils/Constant";
import { FILTER_GENDER } from './../Utils/Constant';

const initialState = {
    professions: null,
    filtersSelected: [],
    name: '',
    orderBy: '',
    gender: ''
};


const FilterReducer = (state = initialState, action) => {
    switch (action.type) {
        case FILTER_PROFESSION_LOAD:
            state = {
                ...state, professions: action.filters,
            };
            break;
        case FILTER_SELECTED:
            state = {
                ...state, filtersSelected: action.filters
            };
            break;
        case FILTER_NAME:
            state = {
                ...state, name: action.name
            };
            break;
        case FILTER_ORDER_BY:
            state = {
                ...state, orderBy: action.orderBy
            };
            break;
        case FILTER_GENDER:
            state = {
                ...state, gender: action.gender
            };
            break;
        default:
            state = {
                ...state
            };
    }
    return state;
};

export default FilterReducer;