import { professionFilter } from '../../__test__/const';
import FilterReducer from './FilterReducer';
import { FILTER_PROFESSION_LOAD, FILTER_SELECTED, FILTER_NAME, FILTER_ORDER_BY, FILTER_GENDER } from '../Utils/Constant';

describe('FilterReducer reducer', () => {
    it('should return the initial state', () => {
        expect(FilterReducer(undefined, {})).toEqual(
            {
                professions: null,
                filtersSelected: [],
                name: '',
                orderBy: '',
                gender: ''
            }
        )
    })

    it('should handle FILTER_PROFESSION_LOAD', () => {
        expect(
            FilterReducer([], {
                type: FILTER_PROFESSION_LOAD,
                filters: professionFilter
            })
        ).toEqual(
            {
                professions: professionFilter,
                filtersSelected: undefined,
                name: undefined,
                orderBy: undefined,
                gender: undefined
            }
        )
    })

    it('should handle FILTER_SELECTED', () => {
        expect(
            FilterReducer([], {
                type: FILTER_SELECTED,
                filters: professionFilter[0]
            })
        ).toEqual(
            {
                professions: undefined,
                filtersSelected: professionFilter[0],
                name: undefined,
                orderBy: undefined,
                gender: undefined
            }
        )
    })
    it('should handle FILTER_NAME', () => {
        expect(
            FilterReducer([], {
                type: FILTER_NAME,
                name: 'tom'
            })
        ).toEqual(
            {
                professions: undefined,
                filtersSelected: undefined,
                name: 'tom',
                orderBy: undefined,
                gender: undefined
            }
        )
    })

    it('should handle FILTER_ORDER_BY', () => {
        expect(
            FilterReducer([], {
                type: FILTER_ORDER_BY,
                orderBy: 'age desc'
            })
        ).toEqual(
            {
                professions: undefined,
                filtersSelected: undefined,
                name: undefined,
                orderBy: 'age desc',
                gender: undefined
            }
        )
    })

    it('should handle FILTER_GENDER', () => {
        expect(
            FilterReducer([], {
                type: FILTER_GENDER,
                gender: 'male'
            })
        ).toEqual(
            {
                professions: undefined,
                filtersSelected: undefined,
                name: undefined,
                orderBy: undefined,
                gender: 'male'
            }
        )
    })
})
