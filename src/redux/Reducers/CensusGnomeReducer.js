import { CENSUS_GNOME_LOAD, GNOME_SELECTED } from "../Utils/Constant";

const initialState = {
    all: null,
    gnomeSelected: null
};

const CensusGnomeReducer = (state=initialState, action) => {
    switch (action.type) {
        case CENSUS_GNOME_LOAD:
            state = {
                ...state, all: action.censusGnome
            };
            break;
        case GNOME_SELECTED:
            state = {
                ...state, gnomeSelected: action.gnome
            };
            break;
        default:
            state = {
                ...state
            };
    }
    return state;
};

export default CensusGnomeReducer;