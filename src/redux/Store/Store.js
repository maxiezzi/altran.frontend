import { createStore, combineReducers } from "redux";
import CensusGnomeReducer from '../Reducers/CensusGnomeReducer';
import FilterReducer from './../Reducers/FilterReducer';

//const persistedState = loadState();
const reducers = combineReducers({
  censusGenome: CensusGnomeReducer,
  filters: FilterReducer
})

const store = createStore(reducers,
  //persistedState,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());

export default store;