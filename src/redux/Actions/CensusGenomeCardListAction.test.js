import { censusGnome, professionFilter } from '../../__test__/const';
import { CENSUS_GNOME_LOAD, GNOME_SELECTED, FILTER_PROFESSION_LOAD } from '../Utils/Constant';
import { censusGnomeLoad, filterProfessionLoad, gnomeSelected } from './CensusGenomeCardListAction';

describe('CensusGenomeCardListAction', () => {
    it('should create an action to add a censusGnomeLoad', () => {
        const expectedAction = {
            type: CENSUS_GNOME_LOAD,
            censusGnome: censusGnome
        }
        expect(censusGnomeLoad(censusGnome)).toEqual(expectedAction)
    })

    it('should create an action to add a gnomeSelected', () => {
        const expectedAction = {
            type: GNOME_SELECTED,
            gnome: censusGnome[0]
        }
        expect(gnomeSelected(censusGnome[0])).toEqual(expectedAction)
    })

    it('should create an action to add a filterProfessionLoad', () => {
        const expectedAction = {
            type: FILTER_PROFESSION_LOAD,
            filters: professionFilter
        }
        expect(filterProfessionLoad(professionFilter)).toEqual(expectedAction)
    })
})