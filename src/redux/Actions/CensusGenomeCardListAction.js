import { CENSUS_GNOME_LOAD, GNOME_SELECTED } from "../Utils/Constant"
import { FILTER_PROFESSION_LOAD } from "../Utils/Constant"

export function censusGnomeLoad(censusGnome) {
    return { type: CENSUS_GNOME_LOAD, censusGnome }
};

export function gnomeSelected(gnome) {
    return { type: GNOME_SELECTED, gnome }
};

export function filterProfessionLoad(filters) {
    return { type: FILTER_PROFESSION_LOAD, filters }
};