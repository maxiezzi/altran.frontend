import { professionFilter } from '../../__test__/const';
import { FILTER_SELECTED, FILTER_NAME, FILTER_ORDER_BY, FILTER_GENDER } from '../Utils/Constant';
import { filterSelected, filterName, filterOrderBy, filterGender } from './FilterSeccionAction';

describe('FilterSeccionAction', () => {
    it('should create an action to add a filterSelected', () => {
        const expectedAction = {
            type: FILTER_SELECTED,
            filters: professionFilter
        }
        expect(filterSelected(professionFilter)).toEqual(expectedAction)
    })

    it('should create an action to add a filterName', () => {
        const expectedAction = {
            type: FILTER_NAME,
            name: 'tom'
        }
        expect(filterName('tom')).toEqual(expectedAction)
    })

    it('should create an action to add a filterOrderBy', () => {
        const expectedAction = {
            type: FILTER_ORDER_BY,
            orderBy: 'age desc'
        }
        expect(filterOrderBy('age desc')).toEqual(expectedAction)
    })

    it('should create an action to add a filterGender', () => {
        const expectedAction = {
            type: FILTER_GENDER,
            gender: 'female'
        }
        expect(filterGender('female')).toEqual(expectedAction)
    })
})