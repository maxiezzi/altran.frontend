import { FILTER_SELECTED, FILTER_NAME, FILTER_ORDER_BY, FILTER_GENDER } from "../Utils/Constant"

export function filterSelected(filters) {
    return { type: FILTER_SELECTED, filters }
};

export function filterName(name) {
    return { type: FILTER_NAME, name }
};

export function filterOrderBy(orderBy) {
    return { type: FILTER_ORDER_BY, orderBy }
};

export function filterGender(gender) {
    return { type: FILTER_GENDER, gender }
};